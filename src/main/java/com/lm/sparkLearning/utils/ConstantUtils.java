package com.lm.sparkLearning.utils;


public class ConstantUtils {
	public final static String METADATA_BROKER_LIST_VALUE = "10.32.19.41:9092,10.32.19.42:9092,10.32.19.43:9092";
	
	public final static String AUTO_OFFSET_RESET_VALUE = "smallest";
	
	public final static String SERIALIZER_CLASS_VALUE = "kafka.serializer.StringEncoder";
	
	public final static String ORDER_TOPIC = "orderTopic";
	
}
